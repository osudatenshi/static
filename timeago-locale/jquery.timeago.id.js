(function (factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    factory(require('jquery'));
  } else {
    factory(jQuery);
  }
}(function (jQuery) {
  // Bahasa Indonesia
  jQuery.timeago.settings.strings = {
    prefixAgo: "yang lalu",
    prefixFromNow: "dari sekarang",
    suffixAgo: "",
    suffixFromNow: "",
    seconds: "kurang dari semenit",
    minute: "sekitar satu menit",
    minutes: "%d menit",
    hour: "sekitar satu jam",
    hours: "sekitar %d jam",
    day: "satu hari",
    days: "%d hari",
    month: "sekitar sebulan",
    months: "%d bulan",
    year: "sekitar setahun",
    years: "%d tahun",
    wordSeparator: " ",
  };
}));
